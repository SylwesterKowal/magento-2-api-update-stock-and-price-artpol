<?php


namespace Kowal\ApiUpdateStockAndPrice\Api;

use http\QueryString;

interface UpdateStockAndPriceManagementInterface
{

    /**
     * @param string $mass
     * @return mixed
     */
    public function postUpdateStockAndPrice($mass);
}
