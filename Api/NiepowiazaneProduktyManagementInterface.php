<?php


namespace Kowal\ApiUpdateStockAndPrice\Api;

use http\QueryString;

interface NiepowiazaneProduktyManagementInterface
{

    /**
     * @return mixed
     */
    public function getNiepowiazaneProdukty();
}
