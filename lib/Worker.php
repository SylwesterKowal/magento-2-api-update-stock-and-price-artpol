<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-13
 * Time: 23:55
 */

namespace Kowal\ApiUpdateStockAndPrice\lib;


class Worker
{

    /**
     * @var \Kowal\ApiUpdateStockAndPrice\lib\MagentoService
     */
    protected $magentoService;


    public function __construct(
        \Kowal\ApiUpdateStockAndPrice\lib\MagentoService $magentoService
    )
    {
        $this->magentoService = $magentoService;
    }


    public function execute($data = [])
    {
        try {

//            file_put_contents('__dane.txt', print_r($data,true));

            $aktywneMagazyny = $this->magentoService->getActiveWarehauses();

            $count = 0;
            $erros = '';
            $warrning = '';
            foreach ($data as $_data) {

                $count++;
                $sku = $_data['sku'];
                $price = $_data['price'];
                $marza = $_data['marza'];
                $qty = $this->getStockByWarehouse($aktywneMagazyny, $_data['magazyny']);


                if (!$this->magentoService->checkIfSkuExists($sku)) {
                    $warrning .= $count . '. WARNING:: Product  SKU ' . $sku . ' => nie istnieje' . "\n";
                    $this->magentoService->updateStatystyki($_data, 0);
                    continue;
                }
                try {
                    $this->magentoService->_updateStocks($sku, $qty);
                    $this->magentoService->updatePrices($sku, $price, $marza);
                    $this->magentoService->updateStatystyki($_data, 1);
                    $warrning .= $count . '. INFO:: Product  SKU ' . $sku . ' zaktualizowany stan: ' . $qty . "\n";
                } catch (Exception $e) {
                    $erros .= $count . '. ERROR:: While updating  SKU (' . $sku . ') => ' . $e->getMessage() . "\n";
                }

            }
            return $erros . $warrning;
        } catch (Exception $e) {
            return "Błąd " . $e->getTraceAsString() . "\n";
        }
    }

    /**
     * Zwraca stan magazynowy na aktywne Magazyny wybrane wpanelu Magento
     * @param $activWarehouses
     * @param $stanyNaMagazyn
     * @return int
     */
    private function getStockByWarehouse($activWarehouses, $stanyNaMagazyn)
    {
        $stock = 0;

        if (is_array($stanyNaMagazyn)) {
            foreach ($stanyNaMagazyn as $magazynId => $magazyn) {
                if (in_array($magazynId, $activWarehouses)) {
                    $stock = $stock + (int)$magazyn['avail'];
                }
            }
        }
        return $stock;
    }

}