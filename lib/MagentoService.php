<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-13
 * Time: 21:50
 */

namespace Kowal\ApiUpdateStockAndPrice\lib;


class MagentoService
{
    public $store_id = 0;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * MagentoService constructor.
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->resourceConnection = $resourceConnection;
        $this->scopeConfig = $scopeConfig;


    }


    /**
     * @return mixed
     */
    private function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }

    /**
     * @return mixed
     */
    private function _getWriteConnection()
    {
        return $this->_getConnection('core_write');
    }

    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }

    /**
     * @param $attributeCode
     * @return mixed
     */
    private function _getAttributeId($attributeCode)
    {
        $connection = $this->_getReadConnection();
        $sql = "SELECT attribute_id FROM " . $this->_getTableName('eav_attribute') . " WHERE entity_type_id = ? AND attribute_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $this->_getEntityTypeId('catalog_product'),
                $attributeCode
            ]
        );
    }

    /**
     * @param $entityTypeCode
     * @return mixed
     */
    private function _getEntityTypeId($entityTypeCode)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_type_id FROM " . $this->_getTableName('eav_entity_type') . " WHERE entity_type_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $entityTypeCode
            ]
        );
    }

    /**
     * @param $sku
     * @return mixed
     */
    private function _getIdFromSku($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne(
            $sql,
            [
                $sku
            ]
        );
    }

    /**
     * @param $sku
     * @return mixed
     */
    public function checkIfSkuExists($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT COUNT(*) AS count_no FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne($sql, [$sku]);
    }

    /**
     * @param $sku
     * @param $price
     * @param int $storeId
     */
    public function updatePrices($sku, $price, $marza_default = 0)
    {
        $connection = $this->_getWriteConnection();
        $entityId = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId('cost');

        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_decimal') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $price
            ]
        );
        $marza_na_produkt = (int)$this->getAttributeValue($entityId, $this->_getAttributeId('marza'), 'marza');
        if ($marza_na_produkt > 0) {
            $marza = 1 + $marza_na_produkt / 100;
        } else if ($marza_default > 0) {
            $marza = 1 + $marza_default / 100;
        } else {
            $marza = 1 + 30 / 100;
        }
        $price = round($price * $marza, 2);
        $attributeId = $this->_getAttributeId('price');
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_decimal') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $price
            ]
        );
    }

    public function updateDeliveryDate($sku, $delivery_date)
    {
        $connection = $this->_getWriteConnection();
        $entityId = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId('delivery_date');
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_datetime') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $delivery_date
            ]
        );
    }

    public function setProduktChwilowoNieDostepny($entityId, $stockStatus, $connection)
    {
        $attributeId = $this->_getAttributeId('produkt_chwilowo_niedostepny');
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_int') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                !$stockStatus
            ]
        );
    }


    /**
     * @param $data
     * @param $powiazany
     */
    public function updateStatystyki($data, $powiazany)
    {
        $connection = $this->_getWriteConnection();
        $sql = "INSERT INTO " . $this->_getTableName('kowal_integracjaartpol_artpol') .
            " (id, name,price,symbol, cat, url, magazyny,weight,stocks,powiazany,last_update) 
            VALUES (?,?,?,?,?,?,?,?,?,?,?) 
        ON DUPLICATE KEY 
        UPDATE name=VALUES(name),
        price=VALUES(price),
        symbol=VALUES(symbol), 
        cat=VALUES(cat), 
        url=VALUES(url), 
        magazyny=VALUES(magazyny),
        weight=VALUES(weight),
        stocks=VALUES(stocks),
        powiazany=VALUES(powiazany),
        last_update=VALUES(last_update)
        ";
        $result = $connection->query(
            $sql,
            [
                (string)$data['id'],
                (string)$data['name'],
                (float)$data['price'],
                (string)$data['symbol'],
                (string)$data['breadcrumbtrail'],
                (string)$data['url'],
                $this->parseMagazyny($data['magazyny']),
                $data['weight'],
                (int)$data['qty'],
                $powiazany,
                date('Y-m-d H:i:s')
            ]
        );

        $this->updateMagazyny($data['magazyny']);
    }


    /**
     * Aktualizacja listy magazynów Artpol do wyboru aktywnych dla których mają być zliczne stany na nsklep
     * @param $magazyny
     */
    private function updateMagazyny($magazyny)
    {
        $connection = $this->_getWriteConnection();


        if (is_array($magazyny)) {
            foreach ($magazyny as $magazynId => $magazyn) {
                $sql = "INSERT INTO " . $this->_getTableName('kowal_integracjaartpol_magazyny') . " (id, Nazwa) VALUES (?, ?) ON DUPLICATE KEY UPDATE Nazwa=VALUES(Nazwa)";
                $connection->query(
                    $sql,
                    [$magazynId, $magazyn['name']]
                );
            }
        }


    }

    /**
     * Treść kolumny Magazyny w podglądzie Artpol
     * @param $magazyny
     * @return string
     */
    private function parseMagazyny($magazyny)
    {
        $magHtml = '';
        if (is_array($magazyny)) {
            foreach ($magazyny as $magazynId => $magazyn) {
                $magHtml .= "({$magazyn['avail']}) - {$magazyn['name']} \n";
            }
        }
        return $magHtml;
    }

    /**
     * @param $sku
     * @param $newQty
     */
    public function _updateStocks($sku, $newQty)
    {
        $connection = $this->_getWriteConnection();
        $productId = $this->_getIdFromSku($sku);

        $sql = "UPDATE " . $this->_getTableName('cataloginventory_stock_item') . " csi
	                   SET
					   csi.qty = ?,
					   csi.manage_stock = 1,
					   csi.use_config_manage_stock = 0,
					   csi.is_in_stock = ?
					   WHERE
					   csi.product_id = ?";

        $sql2 = "UPDATE 
					   " . $this->_getTableName('cataloginventory_stock_status') . " css
	                   SET
	                   css.qty = ?,
					   css.stock_status = ?
					   WHERE
					   css.product_id = ?";


        $isInStock = $newQty > 2 ? 1 : 0;  // jeśli newQty większe od 2. Indywidualne ustawienie dla lampywcentrum
        $stockStatus = $newQty > 2 ? 1 : 0; // jeśli newQty większe od 2. Indywidualne ustawienie dla lampywcentrum

        $connection->query($sql, array($newQty, $isInStock, $productId));
        $connection->query($sql2, array($newQty, $stockStatus, $productId));

        $this->setProduktChwilowoNieDostepny($productId, $stockStatus, $connection);
//        if ($sku == 'artpol_G93307') {
//            file_put_contents('__update.txt', "Update {$sku}: ProductId=" . $productId . " Ilosc=" . $newQty);
//            file_put_contents('__query.txt', $sql);
//        }

        // ustawienie sprzedazy kompletów
        $this->setMinimumQtyToSale($productId, $connection);
    }


    /**
     * Minimalna ilość dodawana do koszyka / komplet
     * @param $productId
     * @param $connection
     */
    public function setMinimumQtyToSale($productId, $connection)
    {
        $minimalna_ilosc_w_koszyku = (int)$this->getAttributeValue($productId, $this->_getAttributeId('minimalna_ilosc_w_koszyku'), 'minimalna_ilosc_w_koszyku');

        if ($minimalna_ilosc_w_koszyku == null || $minimalna_ilosc_w_koszyku = 0 || $minimalna_ilosc_w_koszyku == 1) {
            // wyłączamy sprzedaż w kompletach
            $sql = "UPDATE " . $this->_getTableName('cataloginventory_stock_item') . " csi
	                   SET
					   csi.min_sale_qty = 1,
					   csi.use_config_min_sale_qty = 1,
					   csi.enable_qty_increments = 0,
					   csi.use_config_qty_increments = 1,
					   csi.qty_increments = 0
					   WHERE
					   csi.product_id = ?";
        } else if ($minimalna_ilosc_w_koszyku > 1) {
            // włączamy sprzedaż w kompletach
            $sql = "UPDATE " . $this->_getTableName('cataloginventory_stock_item') . " csi
	                   SET
					   csi.min_sale_qty = $minimalna_ilosc_w_koszyku,
					   csi.use_config_min_sale_qty = 0,
					   csi.enable_qty_increments = 1,
					   csi.use_config_qty_increments = 0,
					   csi.qty_increments = $minimalna_ilosc_w_koszyku,
					   WHERE
					   csi.product_id = ?";
        }

        $connection->query($sql, array($productId));
    }


    /**
     * Zwraca tablicę aktywnych magazynów
     * @return array
     */
    public function getActiveWarehauses()
    {
        return explode(",", $this->scopeConfig->getValue('artpol/options/magazyny', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
    }

    function getAttributeValue($productId, $attributeId, $attributeKey)
    {
        try {
            $attribute_type = $this->_getAttributeType($attributeKey);

            if ($attribute_type == 'static') return false;

            $connection = $this->_getConnection('core_write');

            $sql = "SELECT value FROM " . $this->_getTableName('catalog_product_entity_' . $attribute_type) . " cped
			WHERE  cped.attribute_id = ?
			AND cped.entity_id = ?";
            return $connection->fetchOne($sql, array($attributeId, $productId));

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function _getAttributeType($attribute_code = 'price')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT backend_type
				FROM " . $this->_getTableName('eav_attribute') . "
			WHERE
				entity_type_id = ?
				AND attribute_code = ?";
        $entity_type_id = $this->_getEntityTypeId('catalog_product');
        return $connection->fetchOne($sql, array($entity_type_id, $attribute_code));
    }
}