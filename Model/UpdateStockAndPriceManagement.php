<?php


namespace Kowal\ApiUpdateStockAndPrice\Model;

class UpdateStockAndPriceManagement implements \Kowal\ApiUpdateStockAndPrice\Api\UpdateStockAndPriceManagementInterface
{
    /**
     * @var \Kowal\ApiUpdateStockAndPrice\lib\Worker
     */
    protected $worker;


    public function __construct(
        \Kowal\ApiUpdateStockAndPrice\lib\Worker $worker
    )
    {
        $this->worker = $worker;
    }

    /**
     * @param string $mass
     * @return mixed|string
     */
    public function postUpdateStockAndPrice($mass)
    {
        return $this->worker->execute(json_decode($mass, true));
//        return 'hello api POST return the $mass :' . $mass;
    }
}
