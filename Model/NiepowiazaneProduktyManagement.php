<?php


namespace Kowal\ApiUpdateStockAndPrice\Model;

class NiepowiazaneProduktyManagement implements \Kowal\ApiUpdateStockAndPrice\Api\NiepowiazaneProduktyManagementInterface
{
    /**
     * @var \Kowal\ApiUpdateStockAndPrice\lib\Worker
     */
    protected $worker;


    public function __construct(
        \Kowal\ApiUpdateStockAndPrice\lib\Worker $worker
    )
    {
        $this->worker = $worker;
    }

    /**
     * @param string $mass
     * @return mixed|string
     */
    public function getNiepowiazaneProdukty()
    {
        return 'hello api GET return the unmapped products';
    }
}